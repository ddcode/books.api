"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _express = require("express");

var _booksController = require("../controller/booksController");

var _booksController2 = _interopRequireDefault(_booksController);

var _errors = require("../middlewares/errors");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
	var api = (0, _express.Router)();

	api.get("/", (0, _errors.catchAsync)(_booksController2.default.findAll));
	api.get("/:slug", (0, _errors.catchAsync)(_booksController2.default.findOne));

	api.post("/", (0, _errors.catchAsync)(_booksController2.default.create));

	api.put("/:slug", (0, _errors.catchAsync)(_booksController2.default.update));

	api.delete("/:slug", (0, _errors.catchAsync)(_booksController2.default.remove));

	return api;
};