"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

var _mongooseUrlSlugs = require("mongoose-url-slugs");

var _mongooseUrlSlugs2 = _interopRequireDefault(_mongooseUrlSlugs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Book = _mongoose2.default.Schema({
	title: String,
	author: String,
	description: String,
	cover: String
}, {
	timestamps: true
});

Book.plugin((0, _mongooseUrlSlugs2.default)("title", {
	field: "slug",
	update: true
}));

exports.default = _mongoose2.default.model("Book", Book);