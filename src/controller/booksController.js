import Book from "../models/books";

export default {
	async findOne(req, res, next) {
		const book = await Book.findOne({ slug: req.params.slug });
		if (!book) return next();

		return res.status(200).send({ data: book });
	},

	async findAll(req, res) {
		const books = await Book.find().sort({ createdAt: "desc" });
		if (!books) return next;

		return res.status(200).send({ data: books });
	},

	async create(req, res) {
		const book = await new Book({
			title: req.body.title,
			author: req.body.author,
			description: req.body.description,
			cover: req.body.cover,
		}).save();

		return res.status(201).send({
			data: book,
			message: "New book added to database!",
		});
	},

	async update(req, res, next) {
		const book = await Book.findOne({ slug: req.params.slug });
		if (!book) return next();

		book.title = req.body.title;
		book.cover = req.body.cover;
		book.author = req.body.author;
		book.description = req.body.description;

		await book.save();

		return res.status(200).send({
			data: book,
			message: `Book ${book.title} was updated!`,
		});
	},

	async remove(req, res, next) {
		const book = await Book.findOne({ slug: req.params.slug });
		if (!book) return next();

		book.remove();

		return res.status(200).send({
			message: `Book ${book.title} was removed properly!`,
		});
	},
};
