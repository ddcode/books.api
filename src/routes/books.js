import { Router } from "express";
import booksController from "../controller/booksController";
import { catchAsync } from "../middlewares/errors";

export default () => {
	const api = Router();

	api.get("/", catchAsync(booksController.findAll));
	api.get("/:slug", catchAsync(booksController.findOne));

	api.post("/", catchAsync(booksController.create));

	api.put("/:slug", catchAsync(booksController.update));

	api.delete("/:slug", catchAsync(booksController.remove));

	return api;
};
