import mongoose from "mongoose";
import URLSlugs from "mongoose-url-slugs";

const Book = mongoose.Schema(
	{
		title: String,
		author: String,
		description: String,
		cover: String,
	},
	{
		timestamps: true,
	},
);

Book.plugin(
	URLSlugs("title", {
		field: "slug",
		update: true,
	}),
);

export default mongoose.model("Book", Book);
